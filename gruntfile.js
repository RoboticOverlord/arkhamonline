/**
 * Created by Owner on 5/25/2014.
 */

module.exports = function (grunt) {
    grunt.loadNpmTasks("grunt-ts");
    grunt.loadNpmTasks("grunt-contrib-watch");

    grunt.initConfig(
        {
            watch: {
                client: {
                    files: ['public/typescripts/**/*.ts', 'public/typescripts/*.ts'],
                    tasks: ['ts:buildClient'],
                    options: {
                        interrupt: true
                    }
                },
                server: {
                    files: ['ArkhamOnlineServer/**/*.ts', 'ArkhamOnlineServer/*.ts'],
                    tasks: ['ts:buildServer'],
                    options: {
                        interrupt: true
                    }
                }
            },
            ts: {
                // A specific target
                buildServer: {
                    // The source TypeScript files, http://gruntjs.com/configuring-tasks#files
                    src: ["ArkhamOnlineServer/**/*.ts", 'ArkhamOnlineServer/*.ts'],
                    // If specified, generate this file that to can use for reference management
                    reference: "reference.ts",
                    // If specified, generate an out.js file which is the merged js file
                    out: 'ArkhamOnlineServer/app.js',
                    // Use to override the default options, http://gruntjs.com/configuring-tasks#options
                    options: {
                        // 'es3' (default) | 'es5'
                        target: 'es5',
                        // 'amd' (default) | 'commonjs'
                        module: 'amd',
                        // true (default) | false
                        sourceMap: true,
                        // true | false (default)
                        declaration: false,
                        // true (default) | false
                        removeComments: true
                    }
                },
                buildClient: {
                    // The source TypeScript files, http://gruntjs.com/configuring-tasks#files
                    src: ["public/typescripts/**/*.ts", "public/typescripts/*.ts"],
                    // The source html files, https://github.com/grunt-ts/grunt-ts#html-2-typescript-support
                    html: ["public/templates/**/*.tpl.html"],
                    // If specified, generate this file that to can use for reference management
                    reference: "public/typescripts/reference.ts",
                    // If specified, generate an out.js file which is the merged js file
                    out: 'public/javascripts/client.js',
                    // If specified, watches this directory for changes, and re-runs the current target
//                    watch: 'public/typescripts',
                    // Use to override the default options, http://gruntjs.com/configuring-tasks#options
                    options: {
                        // 'es3' (default) | 'es5'
                        target: 'es3',
                        // 'amd' (default) | 'commonjs'
                        module: 'amd',
                        // true (default) | false
                        sourceMap: true,
                        // true | false (default)
                        declaration: false,
                        // true (default) | false
                        removeComments: true
                    }
                },
                // Another target
                dist: {
                    src: ["test/work/**/*.ts"],
                    // Override the main options for this target
                    options: {
                        sourceMap: false
                    }
                }
            }
        }
    );

    grunt.registerTask("default", ["ts:build"]);
    grunt.registerTask("watcher", ['watch:client']);
};