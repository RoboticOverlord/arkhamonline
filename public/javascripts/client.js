var Exceptions;
(function (Exceptions) {
    var Exception = (function () {
        function Exception(message, innerException) {
            this.message = message;
            this.innerException = innerException;
        }
        return Exception;
    })();
    Exceptions.Exception = Exception;
})(Exceptions || (Exceptions = {}));
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var Exceptions;
(function (Exceptions) {
    var NotImplementedException = (function (_super) {
        __extends(NotImplementedException, _super);
        function NotImplementedException(message, innerException) {
            if (typeof message === "undefined") { message = "Method not implemented."; }
            if (typeof innerException === "undefined") { innerException = null; }
            _super.call(this, message, innerException);
        }
        return NotImplementedException;
    })(Exceptions.Exception);
    Exceptions.NotImplementedException = NotImplementedException;
})(Exceptions || (Exceptions = {}));
var Game = (function () {
    function Game(Canvas) {
        this.Canvas = Canvas;
        this.MouseOver = null;
        this.RenderContext = this.Canvas.getContext("2d");

        this.GameObjects = [];

        this.GameObjects.push(new Boards.MainBoard(new Coordinate(0, 0), new Coordinate(this.Canvas.width, this.Canvas.height)));

        this.Resize();

        this.GameLoop();
    }
    Game.prototype.MouseMove = function () {
        var MouseLocation = new Coordinate(window.event.clientX, window.event.clientY);
        if (this.MouseOver != null) {
            if (this.MouseOver.InsideBoundary(MouseLocation)) {
                this.MouseOver.MouseMove(MouseLocation);
            } else {
                this.MouseOver.MouseOut(MouseLocation);
                this.MouseOver = null;
            }
        }
        for (var i = 0; i < this.GameObjects.length; i++) {
            if (this.GameObjects[i].InsideBoundary(MouseLocation)) {
                if (this.MouseOver !== this.GameObjects[i]) {
                    this.GameObjects[i].MouseIn(MouseLocation);
                }

                this.GameObjects[i].MouseMove(MouseLocation);

                this.MouseOver = this.GameObjects[i];
            }
        }
    };

    Game.prototype.Resize = function () {
        this.Canvas.width = window.innerWidth;
        this.Canvas.height = window.innerHeight;
        for (var i = 0; i < this.GameObjects.length; i++) {
            switch (true) {
                case typeof this.GameObjects[i].SetTrainDestination !== "undefined":
                    this.GameObjects[i].SetBoundaries(new Coordinate(0, 0), new Coordinate(this.Canvas.width, this.Canvas.height));

                    this.GameObjects[i].SetBoundaries(new Coordinate(0, 0), new Coordinate(this.GameObjects[i].GetWidth(), this.Canvas.height));
            }
        }
    };

    Game.prototype.GameLoop = function () {
        this.RenderContext.clearRect(0, 0, this.Canvas.width, this.Canvas.height);
        var sortedObjects = this.GameObjects.sort(function (a, b) {
            return a.Layer - b.Layer;
        });
        for (var i = 0; i < sortedObjects.length; i++) {
            sortedObjects[i].Draw(this.RenderContext);
        }

        setTimeout(this.GameLoop.bind(this), 1000 / 60);
    };
    return Game;
})();
var Boards;
(function (Boards) {
    var MainBoard = (function () {
        function MainBoard(TopBoundary, BottomBoundary) {
            this.TopBoundary = TopBoundary;
            this.BottomBoundary = BottomBoundary;
            this.Layer = 0;
            this.MagnifyLocation = null;
            this.Source = "images/MainBoard.jpg";
            this.Element = new Image();
            this.Element.src = this.Source;
        }
        MainBoard.prototype.GetHeight = function () {
            return (this.BottomBoundary.y - this.TopBoundary.y);
        };

        MainBoard.prototype.GetWidth = function () {
            var viewHeight = (this.BottomBoundary.y - this.TopBoundary.y);
            return viewHeight * (this.Element.width / this.Element.height);
        };

        MainBoard.prototype.GetLocations = function () {
            throw new Exceptions.NotImplementedException();
        };

        MainBoard.prototype.SetBoundaries = function (TopBoundary, BottomBoundary) {
            this.TopBoundary = TopBoundary;
            this.BottomBoundary = BottomBoundary;
        };

        MainBoard.prototype.InsideBoundary = function (Location) {
            return ((Location.x > this.TopBoundary.x && Location.x < this.BottomBoundary.x) && (Location.y > this.TopBoundary.y && Location.y < this.BottomBoundary.y));
        };

        MainBoard.prototype.SetTrainDestination = function (LinkedLocation) {
            throw new Exceptions.NotImplementedException();
        };

        MainBoard.prototype.MouseMove = function (Location) {
            var translatedLocation = new Coordinate(Location.x - this.TopBoundary.x, Location.y - this.TopBoundary.y);
            if (translatedLocation.x < this.GetWidth() && translatedLocation.y < this.GetHeight()) {
                this.MagnifyLocation = translatedLocation;
            }
        };

        MainBoard.prototype.MouseIn = function (Location) {
        };

        MainBoard.prototype.MouseOut = function (Location) {
            this.MagnifyLocation = null;
        };

        MainBoard.prototype.Draw = function (RenderContext) {
            var viewHeight = (this.BottomBoundary.y - this.TopBoundary.y);
            var viewWidth = (this.BottomBoundary.x - this.TopBoundary.x);

            var scaleHeight = viewHeight;
            var scaleWidth = viewHeight * (this.Element.width / this.Element.height);

            RenderContext.clearRect(this.TopBoundary.x, this.TopBoundary.y, viewHeight, viewWidth);
            RenderContext.drawImage(this.Element, this.TopBoundary.x, this.TopBoundary.y, scaleWidth, scaleHeight);

            if (this.MagnifyLocation != null) {
                var unscaled = ToOriginalCoord(this.MagnifyLocation);

                var x = this.MagnifyLocation.x;
                var y = this.MagnifyLocation.y;

                if ((x + 100) > this.GetWidth()) {
                    x = this.GetWidth() - 100;
                }

                if ((y + 100) > this.GetHeight()) {
                    y = this.GetHeight() - 100;
                }

                if (x < 100) {
                    x = 100;
                }
                if (y < 100) {
                    y = 100;
                }

                if (unscaled.x + 100 > this.Element.width) {
                    unscaled.x = this.Element.width - 100;
                }
                if (unscaled.y + 100 > this.Element.height) {
                    unscaled.y = this.Element.height - 100;
                }

                if (unscaled.x < 100) {
                    unscaled.x = 100;
                }
                if (unscaled.y < 100) {
                    unscaled.y = 100;
                }

                RenderContext.drawImage(this.Element, unscaled.x - 100, unscaled.y - 100, 200, 200, x - 100, y - 100, 200, 200);

                RenderContext.strokeRect(x - 100, y - 100, 200, 200);
            }
        };
        return MainBoard;
    })();
    Boards.MainBoard = MainBoard;
})(Boards || (Boards = {}));
var boardImg = new Image();
boardImg.src = "images/MainBoard.jpg";

var Coordinate = (function () {
    function Coordinate(x, y) {
        this.x = x;
        this.y = y;
    }
    return Coordinate;
})();
var UserMouse = new Coordinate(0, 0);
var TheGame;
function main() {
    var canvas = document.getElementById("GameWindow");
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;
    TheGame = new Game(canvas);
    window.onresize = TheGame.Resize.bind(TheGame);
    canvas.onmousemove = TheGame.MouseMove.bind(TheGame);
}

function resizeCanvas() {
}

function Draw(forceDraw) {
    var canvas = document.getElementById("GameWindow");

    var renderContext = canvas.getContext("2d");

    renderContext.clearRect(0, 0, window.innerWidth, window.innerHeight);
    renderContext.drawImage(boardImg, 0, 0, window.innerHeight * (boardImg.width / boardImg.height), window.innerHeight);

    if (UserMouse.x < (window.innerHeight * (boardImg.width / boardImg.height))) {
        var unscaled = ToOriginalCoord(UserMouse);

        var x = UserMouse.x;
        var y = UserMouse.y;

        if ((x + 100) > (window.innerHeight * (boardImg.width / boardImg.height))) {
            x = (window.innerHeight * (boardImg.width / boardImg.height)) - 100;
        }

        if ((y + 100) > window.innerHeight) {
            y = window.innerHeight - 100;
        }

        if (x < 100) {
            x = 100;
        }
        if (y < 100) {
            y = 100;
        }

        if (unscaled.x + 100 > boardImg.width) {
            unscaled.x = boardImg.width - 100;
        }
        if (unscaled.y + 100 > boardImg.height) {
            unscaled.y = boardImg.height - 100;
        }

        if (unscaled.x < 100) {
            unscaled.x = 100;
        }
        if (unscaled.y < 100) {
            unscaled.y = 100;
        }

        renderContext.drawImage(boardImg, unscaled.x - 100, unscaled.y - 100, 200, 200, x - 100, y - 100, 200, 200);

        renderContext.strokeRect(x - 100, y - 100, 200, 200);
    }
    if (!forceDraw)
        setTimeout(Draw, 1000 / 60);
}

function ToOriginalCoord(coords) {
    return new Coordinate(Math.ceil((coords.x / (window.innerHeight * (boardImg.width / boardImg.height))) * boardImg.width), Math.ceil((coords.y / window.innerHeight) * boardImg.height));
}

window.onload = main;
//# sourceMappingURL=client.js.map
