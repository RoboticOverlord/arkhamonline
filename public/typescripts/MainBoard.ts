/**
 * Created by Owner on 5/31/2014.
 */
/// <reference path="reference.ts" />
module Boards {
    export class MainBoard implements IBoard, IGameObject {
        private Source:string;
        private Element:HTMLImageElement;
        public Layer:number = 0;

        private MagnifyLocation:Coordinate = null;

        GetHeight():number {
            return (this.BottomBoundary.y - this.TopBoundary.y);
        }

        GetWidth():number {
            var viewHeight:number = (this.BottomBoundary.y - this.TopBoundary.y);
            return viewHeight * (this.Element.width / this.Element.height);
        }

        GetLocations():any {
            throw new Exceptions.NotImplementedException();
        }

        SetBoundaries(TopBoundary:Coordinate, BottomBoundary:Coordinate):void {
            this.TopBoundary = TopBoundary;
            this.BottomBoundary = BottomBoundary;
        }

        InsideBoundary(Location:Coordinate):boolean {
            return (
                (Location.x > this.TopBoundary.x && Location.x < this.BottomBoundary.x) &&
                (Location.y > this.TopBoundary.y && Location.y < this.BottomBoundary.y));
        }

        SetTrainDestination(LinkedLocation:Locations.ILocation):void {
            throw new Exceptions.NotImplementedException();
        }

        MouseMove(Location:Coordinate):void {
            // magnify!

            var translatedLocation:Coordinate = new Coordinate(Location.x - this.TopBoundary.x, Location.y - this.TopBoundary.y);
            if (translatedLocation.x < this.GetWidth() && translatedLocation.y < this.GetHeight()) {
                this.MagnifyLocation = translatedLocation;
            }
        }

        MouseIn(Location:Coordinate):void { }

        MouseOut(Location:Coordinate):void {
            this.MagnifyLocation = null;
        }

        Draw(RenderContext:CanvasRenderingContext2D):void {

            var viewHeight:number = (this.BottomBoundary.y - this.TopBoundary.y);
            var viewWidth:number = (this.BottomBoundary.x - this.TopBoundary.x);

            var scaleHeight:number = viewHeight;
            var scaleWidth:number = viewHeight * (this.Element.width / this.Element.height);

            RenderContext.clearRect(this.TopBoundary.x, this.TopBoundary.y, viewHeight, viewWidth);
            RenderContext.drawImage(this.Element, this.TopBoundary.x, this.TopBoundary.y, scaleWidth, scaleHeight);

            if (this.MagnifyLocation != null) {
                // draw the magnification
                var unscaled:Coordinate = ToOriginalCoord(this.MagnifyLocation);

                var x:number = this.MagnifyLocation.x;
                var y:number = this.MagnifyLocation.y;

                if ((x + 100) > this.GetWidth()) {
                    x = this.GetWidth() - 100;
                }

                if ((y + 100) > this.GetHeight()) {
                    y = this.GetHeight() - 100;
                }

                if (x < 100) {
                    x = 100;
                }
                if (y < 100) {
                    y = 100;
                }

                if (unscaled.x + 100 > this.Element.width) {
                    unscaled.x = this.Element.width - 100;
                }
                if (unscaled.y + 100 > this.Element.height) {
                    unscaled.y = this.Element.height - 100;
                }

                if (unscaled.x < 100) {
                    unscaled.x = 100;
                }
                if (unscaled.y < 100) {
                    unscaled.y = 100;
                }

                RenderContext.drawImage(this.Element, unscaled.x - 100, unscaled.y - 100, 200, 200, x - 100, y - 100, 200, 200);

                RenderContext.strokeRect(x - 100, y - 100, 200, 200);
            }
        }

        constructor(public TopBoundary:Coordinate, public BottomBoundary:Coordinate) {
            this.Source = "images/MainBoard.jpg";
            this.Element = new Image();
            this.Element.src = this.Source;

        }
    }
}