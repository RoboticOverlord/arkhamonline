/**
 * Created by Owner on 5/31/2014.
 */

var boardImg:HTMLImageElement = new Image();
boardImg.src = "images/MainBoard.jpg";


class Coordinate {
    constructor(public x:number, public y:number) {
    }
}
var UserMouse:Coordinate = new Coordinate(0, 0);
var TheGame:Game;
function main() {
    var canvas:HTMLCanvasElement = <HTMLCanvasElement>document.getElementById("GameWindow");
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;
    TheGame = new Game(canvas);
    window.onresize = TheGame.Resize.bind(TheGame);
    canvas.onmousemove = TheGame.MouseMove.bind(TheGame);
//    resizeCanvas();
//    var canvas:HTMLCanvasElement = <HTMLCanvasElement>document.getElementById("GameWindow");
//    var renderContext:CanvasRenderingContext2D = canvas.getContext("2d");
//
//    canvas.onmousemove = () => {
//        UserMouse.x = window.event.clientX;
//        UserMouse.y = window.event.clientY;
//    };
//
//    var boardImg:HTMLImageElement = new Image();
//    boardImg.onload = function () {
//        renderContext.drawImage(boardImg, 0, 0, window.innerHeight * (boardImg.width / boardImg.height), window.innerHeight);
//    };
//
//    setTimeout(Draw, 1000 / 60);
}

function resizeCanvas() {
    /*var canvas:HTMLCanvasElement = <HTMLCanvasElement>document.getElementById("GameWindow");
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;
    Draw(true);*/
}

function Draw(forceDraw:boolean) {
    var canvas:HTMLCanvasElement = <HTMLCanvasElement>document.getElementById("GameWindow");

    var renderContext:CanvasRenderingContext2D = canvas.getContext("2d");

    renderContext.clearRect(0, 0, window.innerWidth, window.innerHeight);
    renderContext.drawImage(boardImg, 0, 0, window.innerHeight * (boardImg.width / boardImg.height), window.innerHeight);


    if (UserMouse.x < (window.innerHeight * (boardImg.width / boardImg.height))) {

        var unscaled:Coordinate = ToOriginalCoord(UserMouse);

        var x:number = UserMouse.x;
        var y:number = UserMouse.y;

        if ((x + 100) > (window.innerHeight * (boardImg.width / boardImg.height))) {
            x = (window.innerHeight * (boardImg.width / boardImg.height)) - 100;
        }

        if ((y + 100) > window.innerHeight) {
            y = window.innerHeight - 100;
        }

        if (x < 100) {
            x = 100;
        }
        if (y < 100) {
            y = 100;
        }

        if (unscaled.x + 100 > boardImg.width) {
            unscaled.x = boardImg.width - 100;
        }
        if (unscaled.y + 100 > boardImg.height) {
            unscaled.y = boardImg.height - 100;
        }

        if (unscaled.x < 100) {
            unscaled.x = 100;
        }
        if (unscaled.y < 100) {
            unscaled.y = 100;
        }

        renderContext.drawImage(boardImg, unscaled.x - 100, unscaled.y - 100, 200, 200, x - 100, y - 100, 200, 200);

        renderContext.strokeRect(x - 100, y - 100, 200, 200);
    }
    if (!forceDraw)
        setTimeout(Draw, 1000 / 60);
}

function ToOriginalCoord(coords:Coordinate) {
    return new Coordinate(Math.ceil((coords.x / (window.innerHeight * (boardImg.width / boardImg.height))) * boardImg.width), Math.ceil((coords.y / window.innerHeight) * boardImg.height));
}

window.onload = main;
//window.onresize = resizeCanvas;