/**
 * Created by Owner on 6/1/2014.
 */

interface IGameObject {
    Layer:number;
    GetHeight():number;
    GetWidth():number;
    SetBoundaries(TopBoundary:Coordinate, BottomBoundary:Coordinate):void;
    InsideBoundary(Location:Coordinate):boolean;
    MouseMove(Location:Coordinate):void;
    MouseIn(Location:Coordinate):void;
    MouseOut(Location:Coordinate):void;
    Draw(RenderContext:CanvasRenderingContext2D):void;
}