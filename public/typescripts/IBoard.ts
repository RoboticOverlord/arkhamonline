/**
 * Created by Owner on 5/31/2014.
 */
/// <reference path="reference.ts" />
module Boards {
    export interface IBoard {
        GetLocations():any;
        SetTrainDestination(LinkedLocation:Locations.ILocation):void;
    }
}