/**
 * Created by Owner on 5/31/2014.
 */
/// <reference path="../reference.ts" />
module Exceptions {
    export class NotImplementedException extends Exception {
        constructor(message:string = "Method not implemented.", innerException:any = null) {
            super(message, innerException);
        }
    }
}