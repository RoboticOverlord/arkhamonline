/**
 * Created by Owner on 5/31/2014.
 */

module Exceptions {
    export class Exception {
        constructor(public message:string, public innerException:any) {

        }
    }
}