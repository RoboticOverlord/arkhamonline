/**
 * Created by Owner on 6/1/2014.
 */

class Game {

    GameObjects:IGameObject[];
    RenderContext:CanvasRenderingContext2D;
    private MouseOver:IGameObject = null;

    constructor(public Canvas:HTMLCanvasElement) {

        this.RenderContext = this.Canvas.getContext("2d");

        this.GameObjects = [];

        // setup the main board
        this.GameObjects.push(new Boards.MainBoard(new Coordinate(0, 0), new Coordinate(this.Canvas.width, this.Canvas.height)));

        // get the initial size and boundaries set up.
        this.Resize();

        // begin the game loop
        this.GameLoop();
    }

    MouseMove():void {
        var MouseLocation:Coordinate = new Coordinate(window.event.clientX, window.event.clientY);
        if(this.MouseOver != null) {
            if (this.MouseOver.InsideBoundary(MouseLocation)) {
                this.MouseOver.MouseMove(MouseLocation);
            }
            else {
                this.MouseOver.MouseOut(MouseLocation);
                this.MouseOver = null;
            }
        }
        for(var i = 0; i < this.GameObjects.length; i++) {
            if(this.GameObjects[i].InsideBoundary(MouseLocation)) {
                if(this.MouseOver !== this.GameObjects[i]) {
                    this.GameObjects[i].MouseIn(MouseLocation);
                }

                this.GameObjects[i].MouseMove(MouseLocation);

                this.MouseOver = this.GameObjects[i];
            }
        }
    }

    Resize():void {
        this.Canvas.width = window.innerWidth;
        this.Canvas.height = window.innerHeight;
        for(var i = 0; i < this.GameObjects.length; i++) {
            // switch to determine what type of location we're dealing with
            switch(true) {
                // game boards go on the left
                case typeof (<any>this.GameObjects[i]).SetTrainDestination !== "undefined":
                    this.GameObjects[i].SetBoundaries(new Coordinate(0, 0), new Coordinate(this.Canvas.width, this.Canvas.height));
                    // this sucks, but i want the right boundary to be the board width
                    this.GameObjects[i].SetBoundaries(new Coordinate(0, 0), new Coordinate(this.GameObjects[i].GetWidth(), this.Canvas.height));
            }
        }
        //Draw(true);
    }

    GameLoop():void {
        this.RenderContext.clearRect(0, 0, this.Canvas.width, this.Canvas.height);
        var sortedObjects:IGameObject[] = (<IGameObject[]>this.GameObjects).sort((a, b) => a.Layer - b.Layer);
        for (var i = 0; i < sortedObjects.length; i++) {
            sortedObjects[i].Draw(this.RenderContext);
        }
        // 60fps sorta
        setTimeout(this.GameLoop.bind(this), 1000 / 60);
    }
}